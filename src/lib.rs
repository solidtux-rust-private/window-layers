use anyhow::Result;
use regex::Regex;
use schemars::JsonSchema;
use serde::Deserialize;
use swayipc::{WindowChange, WindowEvent};

#[repr(u8)]
#[derive(Clone, Copy, Debug, Deserialize, JsonSchema)]
#[allow(unused)]
#[serde(rename_all = "snake_case")]
pub enum Layer {
    Qwerty = 1,
    Game = 2,
    GameArrows = 4,
    GameBoth = 8,
}

#[derive(Clone, Debug, Deserialize, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum Filter {
    // Fields
    Name(String),
    AppId(String),
    Title(String),
    Change(Change),
    Instance(String),
    // Operations
    Not(Box<Filter>),
    Any(Vec<Filter>),
    All(Vec<Filter>),
}

#[derive(Clone, Debug, Deserialize, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum Change {
    New,
    Close,
    Focus,
    Title,
    FullscreenMode,
    Move,
    Floating,
    Urgent,
    Mark,
}

impl From<&Change> for WindowChange {
    fn from(value: &Change) -> Self {
        match value {
            Change::New => WindowChange::New,
            Change::Close => WindowChange::Close,
            Change::Focus => WindowChange::Focus,
            Change::Title => WindowChange::Title,
            Change::FullscreenMode => WindowChange::FullscreenMode,
            Change::Move => WindowChange::Move,
            Change::Floating => WindowChange::Floating,
            Change::Urgent => WindowChange::Urgent,
            Change::Mark => WindowChange::Mark,
        }
    }
}

impl Filter {
    pub fn matches(&self, event: &WindowEvent) -> Result<bool> {
        Ok(match self {
            Filter::Name(re) => {
                let r = Regex::new(re)?;
                event
                    .container
                    .name
                    .as_ref()
                    .map(|x| r.is_match(x))
                    .unwrap_or_default()
            }
            Filter::AppId(re) => {
                let r = Regex::new(re)?;
                event
                    .container
                    .app_id
                    .as_ref()
                    .map(|x| r.is_match(x))
                    .unwrap_or_default()
            }
            Filter::Title(re) => {
                let r = Regex::new(re)?;
                event
                    .container
                    .window_properties
                    .as_ref()
                    .and_then(|x| x.title.as_deref())
                    .map(|x| r.is_match(x))
                    .unwrap_or_default()
            }
            Filter::Instance(re) => {
                let r = Regex::new(re)?;
                event
                    .container
                    .window_properties
                    .as_ref()
                    .and_then(|x| x.instance.as_deref())
                    .map(|x| r.is_match(x))
                    .unwrap_or_default()
            }
            Filter::All(v) => v
                .iter()
                .map(|x| x.matches(event))
                .collect::<Result<Vec<bool>>>()?
                .into_iter()
                .all(|x| x),
            Filter::Any(v) => v
                .iter()
                .map(|x| x.matches(event))
                .collect::<Result<Vec<bool>>>()?
                .into_iter()
                .any(|x| x),
            Filter::Not(f) => !f.matches(event)?,
            Filter::Change(c) => {
                let wc: WindowChange = c.into();
                event.change == wc
            }
        })
    }
}

#[derive(Clone, Debug, Deserialize, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum Action {
    SetLayer(Layer),
}

#[derive(Clone, Debug, Deserialize, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub struct Rule {
    pub filter: Filter,
    pub action: Action,
    pub stop: bool,
}

#[derive(Clone, Debug, Deserialize, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub struct Config {
    pub rules: Vec<Rule>,
}
