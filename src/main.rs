use std::fs::File;

use anyhow::{anyhow, Result};
use directories::ProjectDirs;
use planck::{Command, Keyboard};
use swayipc::{Connection, Event, EventType, WindowChange};
use window_layers::{Action, Config};

fn main() -> Result<()> {
    let proj_dirs = ProjectDirs::from("de", "solidtux", "window-layers")
        .ok_or(anyhow!("Could not get project directories."))?;
    let config: Config = {
        let mut conf_path = proj_dirs.config_dir().to_path_buf();
        conf_path.push("config.json");
        serde_json::from_reader(File::open(conf_path)?)?
    };

    let mut keyboard = Keyboard::open()?;

    let conn = Connection::new()?.subscribe([EventType::Window])?;

    for event in conn {
        if let Event::Window(window) = event? {
            if window.change == WindowChange::Focus {
                println!("{:?}", window.container);
                for rule in &config.rules {
                    let matches = rule.filter.matches(&window)?;
                    if matches {
                        match &rule.action {
                            Action::SetLayer(layer) => {
                                keyboard.send(Command::Layer(*layer as u8))?
                            }
                        }
                    }
                    if matches && rule.stop {
                        break;
                    }
                }
            }
        }
    }
    Ok(())
}
