use schemars::schema_for;
use window_layers::Config;

fn main() {
    let schema = schema_for!(Config);
    println!("{}", serde_json::to_string_pretty(&schema).unwrap());
}
